package net.voronoff.seaworld.ui;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.DrawableRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayout;
import android.util.Log;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.Space;

import net.voronoff.seaworld.R;
import net.voronoff.seaworld.core.Organism;
import net.voronoff.seaworld.core.World;
import net.voronoff.seaworld.core.WorldBuilder;
import net.voronoff.seaworld.core.organism.Grampus;
import net.voronoff.seaworld.core.organism.Penguin;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private static final int HEIGHT = 15;
    private static final int WIDTH = 10;

    private World sea;

    @BindView(R.id.place)
    GridLayout place;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initSea();
    }

    @OnClick(R.id.place)
    void onClickPlace() {
        if (sea != null) {
            sea.update();
            convert(sea);
        }
    }

    @OnClick(R.id.restart)
    void onClickRestart() {
        initSea();
    }

    @OnClick(R.id.auto)
    void onClickAuto() {
        final Handler handler = new Handler();
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (!sea.update()) {
                    timer.cancel();
                }
                handler.post(() -> convert(sea));
            }
        }, 0, 250);
    }

    private void initSea() {
        sea = new WorldBuilder(HEIGHT, WIDTH)
                .populatePenguin(50.0f)
                .populateGrampus(5.0f)
                .create();

        place.setRowCount(sea.getHeight());
        place.setColumnCount(sea.getWidth());

        convert(sea);
    }

    private void convert(World sea) {
        if (sea == null) {
            Snackbar.make(place, "Before need create world", Snackbar.LENGTH_SHORT).show();
            return;
        }

        place.removeAllViews();

        sea.foreach(cell -> {
            GridLayout.Spec specRow = GridLayout.spec(cell.getRow(), 1.0f);
            GridLayout.Spec specColumn = GridLayout.spec(cell.getColumn(), 1.0f);
            GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(specRow, specColumn);
            layoutParams.setGravity(Gravity.FILL);
            layoutParams.width = 0;
            layoutParams.height = 0;

            Organism organism = cell.getOrganism();
            if (organism == null) {
                place.addView(new Space(this), layoutParams);
            } else if (organism.getClass() == Penguin.class) {
                place.addView(getImageView(R.drawable.tux), layoutParams);
            } else if (organism.getClass() == Grampus.class) {
                place.addView(getImageView(R.drawable.orca), layoutParams);
            } else {
                Log.w(TAG, "Oops!");
            }
        });

//        Log.d(TAG, sea.toString());
    }

    private ImageView getImageView(@DrawableRes int id) {
        ImageView image = new ImageView(this);
        image.setScaleType(ImageView.ScaleType.FIT_CENTER);
        image.setImageResource(id);
        return image;
    }
}
