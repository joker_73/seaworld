package net.voronoff.seaworld.core.util;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

public class Counter {
    private HashMap<Class, Integer> counters = new HashMap<>();

    public void inc(@NotNull Class clazz) {
        counters.put(clazz, get(clazz) + 1);
    }

    public int get(@NotNull Class clazz) {
        Integer counter = counters.get(clazz);
        return counter != null ? counter : 0;
    }
}
