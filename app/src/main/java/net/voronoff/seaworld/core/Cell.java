package net.voronoff.seaworld.core;

import org.jetbrains.annotations.Nullable;

public class Cell {
    private int row;
    private int column;
    private Organism organism;

    public Cell(int row, int column) {
        this(row, column, null);
    }

    public Cell(int row, int column, Organism organism) {
        this.row = row;
        this.column = column;
        this.organism = organism;
    }

    public boolean isEmpty() {
        return organism == null;
    }

    @Nullable
    public Organism getOrganism() {
        return organism;
    }

    public void setOrganism(@Nullable Organism organism) {
        this.organism = organism;
    }

    public Organism moveOrganism(Cell destination) {
        Organism previousOrganism = destination.organism;
        destination.organism = organism;
        organism = null;
        return previousOrganism;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public void vacate() {
        organism = null;
    }
}
