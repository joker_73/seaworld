package net.voronoff.seaworld.core;

import net.voronoff.seaworld.core.organism.Grampus;
import net.voronoff.seaworld.core.organism.Penguin;
import net.voronoff.seaworld.core.util.Consumer;
import net.voronoff.seaworld.core.util.Counter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashSet;

public class World {
    private int height;
    private int width;

    private Cell[][] cells;

    private int tick;

    public World(int height, int width, Cell[][] cells) {
        this.width = width;
        this.height = height;
        this.cells = cells;
    }

    public boolean update() {
        tick++;

        Counter counter = new Counter();

        HashSet<Organism> handledOrganisms = new HashSet<>();

        foreach(cell -> {
            Organism organism = cell.getOrganism();
            if (organism != null && !handledOrganisms.contains(organism)) {
                handledOrganisms.add(organism);
                organism.update(cell, getViewable(cell));
                counter.inc(organism.getClass());
            }
        });

        return counter.get(Grampus.class) != 0;
    }

    @NotNull
    public Cell[][] getCells() {
        return cells;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public int getTick() {
        return tick;
    }

    @NotNull
    private ArrayList<Cell> getViewable(Cell cell) {
        int currentRow = cell.getRow();
        int currentColumn = cell.getColumn();
        ArrayList<Cell> viewable = new ArrayList<>(8);

        for (int i = -1; i < 2; ++i) {
            int row = currentRow + i;
            if (row < 0 || row >= height) {
                continue;
            }
            for (int j = -1; j < 2; ++j) {
                int column = currentColumn + j;
                if (column < 0 || column >= width || (row == currentRow && column == currentColumn)) {
                    continue;
                }
                viewable.add(cells[row][column]);
            }
        }

        return viewable;
    }

    public void foreach(Consumer<Cell> consumer) {
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                consumer.accept(cells[i][j]);
            }
        }
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        Counter counter = new Counter();

        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                Organism organism = cells[i][j].getOrganism();
                if (organism == null) {
                    stringBuilder.append(".");
                    counter.inc(Object.class);
                    continue;
                }
                counter.inc(organism.getClass());
                if (organism.getClass() == Penguin.class) {
                    stringBuilder.append("P");
                } else if (organism.getClass() == Grampus.class) {
                    stringBuilder.append("G");
                } else {
                    stringBuilder.append("#");
                }
            }
            stringBuilder.append("\n");
        }
        stringBuilder.append("Tick: ").append(tick).append(" ::: Grampus: ")
                .append(counter.get(Grampus.class)).append(" ::: Penguin: ")
                .append(counter.get(Penguin.class)).append(" ::: Empty: ")
                .append(counter.get(Object.class)).append(" ::: Total: ").append(height * width)
                .append("\n");

        return stringBuilder.toString();
    }
}
