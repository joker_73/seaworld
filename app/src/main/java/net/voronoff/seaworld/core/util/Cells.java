package net.voronoff.seaworld.core.util;

import net.voronoff.seaworld.core.Cell;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

public class Cells {
    private static Random random = new Random();

    public static boolean moveOrganismToRandomEmptyCell(@NotNull Cell cell, @NotNull Collection<Cell> place) {
        boolean moved;
        Cell emptyCell = getRandomEmptyCell(place);
        if (emptyCell != null) {
            cell.moveOrganism(emptyCell);
            moved = true;
        } else {
            moved = false;
        }
        return moved;
    }

    @Nullable
    public static Cell getRandomEmptyCell(@NotNull Collection<Cell> place) {
        Cell cell = null;
        ArrayList<Cell> emptyCells = getEmptyCells(place);
        if (emptyCells.size() > 0) {
            cell = emptyCells.get(random.nextInt(emptyCells.size()));
        }
        return cell;
    }

    @NotNull
    public static ArrayList<Cell> getEmptyCells(@NotNull Collection<Cell> place) {
        ArrayList<Cell> emptyCells = new ArrayList<>();
        for (Cell cell : place) {
            if (cell.isEmpty()) {
                emptyCells.add(cell);
            }
        }
        return emptyCells;
    }

    @NotNull
    public static ArrayList<Cell> getInhabitedCells(@NotNull Collection<Cell> place) {
        ArrayList<Cell> inhabitedCells = new ArrayList<>();
        for (Cell cell : place) {
            if (!cell.isEmpty()) {
                inhabitedCells.add(cell);
            }
        }
        return inhabitedCells;
    }
}
