package net.voronoff.seaworld.core.organism;

import net.voronoff.seaworld.core.Cell;
import net.voronoff.seaworld.core.Organism;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Random;

import static net.voronoff.seaworld.core.util.Cells.getInhabitedCells;
import static net.voronoff.seaworld.core.util.Cells.moveOrganismToRandomEmptyCell;

public class Grampus extends Organism {
    private static final int FULL = 3;

    private int satiety = FULL;
    private static Random random = new Random();

    @Override
    protected int getProcreatePeriod() {
        return 8;
    }

    @Override
    protected void move(@NotNull Cell currentCell, @NotNull ArrayList<Cell> viewableCells) {
        satiety--;

        ArrayList<Cell> eatableCells = new ArrayList<>();
        for (Cell cell : getInhabitedCells(viewableCells)) {
            Organism organism = cell.getOrganism();
            if (isEatable(organism)) {
                eatableCells.add(cell);
            }
        }

        if (eatableCells.size() > 0) {
            Cell foodCell = eatableCells.get(random.nextInt(eatableCells.size()));
            currentCell.moveOrganism(foodCell);
            satiety = FULL;
        } else if (satiety >= 0) {
            moveOrganismToRandomEmptyCell(currentCell, viewableCells);
        } else {
            currentCell.vacate();
        }
    }

    private boolean isEatable(Organism organism) {
        assert organism != null;

        boolean eatable = false;

        if (organism instanceof Penguin) {
            eatable = true;
        }

        return eatable;
    }
}
