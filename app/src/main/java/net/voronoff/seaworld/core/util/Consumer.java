package net.voronoff.seaworld.core.util;

public interface Consumer<T> {
    void accept(T t);
}
