package net.voronoff.seaworld.core;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;

import static net.voronoff.seaworld.core.util.Cells.getRandomEmptyCell;

public abstract class Organism {
    protected int age;

    public final void update(@NotNull Cell current, @NotNull ArrayList<Cell> viewableCells) {
        age++;
        procreate(viewableCells);
        move(current, viewableCells);
    }

    abstract protected int getProcreatePeriod();
    abstract protected void move(@NotNull Cell currentCell, @NotNull ArrayList<Cell> viewableCells);

    @SuppressWarnings("TryWithIdenticalCatches")
    protected void procreate(@NotNull Collection<Cell> place) {
        if (age % getProcreatePeriod() == 0) {
            Cell cell = getRandomEmptyCell(place);
            if (cell != null) {
                try {
                    Constructor<?> constructor = this.getClass().getConstructor();
                    cell.setOrganism((Organism)constructor.newInstance());
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
