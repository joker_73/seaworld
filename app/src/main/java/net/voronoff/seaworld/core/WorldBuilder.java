package net.voronoff.seaworld.core;

import net.voronoff.seaworld.core.organism.Grampus;
import net.voronoff.seaworld.core.organism.Penguin;

import java.util.LinkedList;
import java.util.Random;

public class WorldBuilder {
    private int height = 15;
    private int width = 10;
    private int penguins;
    private int grampus;

    public WorldBuilder(int height, int width) {
        if (height <= 0 || width <= 0) {
            throw new IllegalArgumentException();
        }
        this.height = height;
        this.width = width;
    }

    public WorldBuilder populatePenguin(float percent) {
        penguins = (int) (height * width * percent / 100);
        return this;
    }

    public WorldBuilder populateGrampus(float percent) {
        grampus = (int) (height * width * percent / 100);
        return this;
    }

    public World create() {
        LinkedList<Cell> emptyCells = new LinkedList<>();

        Cell[][] cells = new Cell[height][width];
        for (int row = 0; row < height; ++row) {
            for (int column = 0; column < width; ++column) {
                Cell cell = new Cell(row, column);
                emptyCells.add(cell);
                cells[row][column] = cell;
            }
        }

        Random random = new Random();

        for (int i = 0; emptyCells.size() > 0 && i < penguins; ++i) {
            emptyCells.remove(random.nextInt(emptyCells.size())).setOrganism(new Penguin());
        }

        for (int i = 0; emptyCells.size() > 0 && i < grampus; ++i) {
            emptyCells.remove(random.nextInt(emptyCells.size())).setOrganism(new Grampus());
        }

        return new World(height, width, cells);
    }
}
