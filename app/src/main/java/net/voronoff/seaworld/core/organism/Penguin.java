package net.voronoff.seaworld.core.organism;

import net.voronoff.seaworld.core.Cell;
import net.voronoff.seaworld.core.Organism;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import static net.voronoff.seaworld.core.util.Cells.moveOrganismToRandomEmptyCell;

public class Penguin extends Organism {

    @Override
    protected int getProcreatePeriod() {
        return 3;
    }

    @Override
    protected void move(@NotNull Cell currentCell, @NotNull ArrayList<Cell> viewableCells) {
        moveOrganismToRandomEmptyCell(currentCell, viewableCells);
    }
}
